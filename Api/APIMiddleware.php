<?php

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;
use \Firebase\JWT\JWT;


class APIMiddleware implements IMiddleware {

    public function handle(Request $request) {
        // Check si le token est valide
        if (!empty($_POST['token'])) {
            $key =  '2WedwWt9m0vDgoNtvm2DV';
            try {
                $decoded = JWT::decode($_POST['token'], $key, array('HS256'));
            } catch (Exception $e) {
                $this->redirectError($request);
            }
        } else {
            $this->redirectError($request);
        }
    }

    private function redirectError ($request) {
        // $url = SimpleRouter::getUrl('error');
        // $request->setRewriteUrl($url);
        $request->setRewriteCallback('SubscriberController@errorLogin');
        return $request;
    }
}