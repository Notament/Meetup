<?php

require_once 'models/Meetup.php';

class MeetupController {

    function getAll() {
        $meetup = new Meetup();
        //SHOW ALL MEETUP
        echo json_encode($meetup->getAll());
    }

    function deleteEvent() {
        echo $_POST['token'];
        $id_event = $_POST['id'];
        $meetup = new Meetup();
        //DELETE EVENT IN DB
        $meetup->deleteEvent($id_event);
    }

    function addEvent() {
        $location = $_POST['location'];
        $meetup = new Meetup();
        //GET TOWN FROM THE FORM
        $id_town = $meetup->getTown($location);
        //INSERT INTO DB
        $meetup->addEvent($_POST['meetup_title'],$_POST['meetup_description'],$_POST['hour'],$_POST['daymonth'],$id_town['id']);
        //GET LAST EVENT TO SHOW IT IN AJAX
        echo json_encode($meetup->getLastEvent());
    }

    function getEventById() {
        $id_event = $_POST['id'];
        $meetup = new Meetup();
        $event = $meetup->getEventById($id_event);
        echo json_encode($event);
    }

    function updateEvent(){
        $location = $_POST['location'];
        $meetup = new Meetup();
        //GET TOWN FROM THE FORM
        $id_town = $meetup->getTown($location);
        //UPDATE EN DB DES NOUVEAUX CHANGEMENTS
        echo $meetup->updateEvent($_POST['meetup_title'],$_POST['meetup_description'],$_POST['hour'],$_POST['daymonth'],$id_town['id'],$_POST['id']);
    }
}