<?php

require_once 'models/Subscriber.php';
use \Firebase\JWT\JWT;

class SubscriberController{

    function loginValidate(){
        $log = $_POST['login'];
        $mdp = $_POST['mdp'];
        $subscriber = new Subscriber;
        $validation = $subscriber->loginValidate($log,$mdp);
        if(empty($validation)){
            http_response_code(401);
        }
        else{

            $key = '2WedwWt9m0vDgoNtvm2DV';

            $data = [
                "user_id" => $validation[0],
                "exp" => time() + 10,
                "iat" => time(),
            ];

            $token = JWT::encode($data,$key);

            echo json_encode(['token'=>$token]);
        }
    }

    function errorLogin(){
            http_response_code(401);
        }
 
}