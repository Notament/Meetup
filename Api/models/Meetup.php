<?php

include './config/config.php';

class Meetup {

    public function getAll() {
        $bdd = connect();
        $bdd = $bdd->query('SELECT meetup.*, location.* FROM meetup INNER JOIN location ON meetup.id_location = location.id ');
        $bdd = $bdd->fetchAll();
        return $bdd;
    }

    public function deleteEvent($id) {
        $bdd = connect();
        $bdd = $bdd->prepare('DELETE FROM meetup WHERE id = :id');
        $bdd->execute(['id'=>$id]);
    }

    function getTown($location){
        $bdd = connect();
        $bdd = $bdd->prepare('SELECT id FROM location WHERE city = :location');
        $bdd->execute(['location'=>$location]);
        $bdd = $bdd->fetch();
        return $bdd;
    }

    function addEvent($title,$description,$hour,$date,$id_town){
        $bdd = connect();
        $bdd = $bdd->prepare('INSERT INTO meetup (meetup_title,meetup_description,meetup_date,meetup_pic,meetup_hour,id_location) VALUES (:title, :description, :date, "./assets/waluigi.jpg" , :hour, :id_location)');
        $bdd->execute(['title'=>$title,'description'=>$description,'date'=>$date,'hour'=>$hour,'id_location'=>$id_town]);
    }

    function getLastEvent(){
        $bdd = connect();
        $bdd = $bdd->query('SELECT meetup.*,location.* FROM meetup INNER JOIN location ON meetup.id_location = location.id ORDER BY meetup.id DESC');
        $bdd = $bdd->fetchAll();
        return $bdd;
    }

    function getEventById($id){
        $bdd = connect();
        $bdd = $bdd->prepare('SELECT meetup.*,location.* FROM meetup INNER JOIN location ON meetup.id_location = location.id  WHERE meetup.id = :id');
        $bdd->execute(['id'=>$id]);
        $bdd = $bdd->fetchAll();
        return $bdd;
        }
    
    function updateEvent($title,$description,$hour,$date,$id_town,$id_event){
        $bdd = connect();
        $bdd = $bdd->prepare('UPDATE meetup SET meetup_title = :title, meetup_description = :description, meetup_date = :date, meetup_hour = :hour, id_location = :id_location WHERE id = :id');
        $bdd->execute(['title'=>$title,'description'=>$description,'date'=>$date,'hour'=>$hour,'id_location'=>$id_town,'id'=>$id_event]);
    }
}
