<?php


use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/MeetupController.php';
require_once 'controllers/LocationController.php';
require_once 'controllers/SubscriberController.php';


// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/Meetup/Api';

SimpleRouter::group(['prefix' => $prefix], function () {
    //Meetup
    SimpleRouter::post('/login', 'SubscriberController@loginValidate');
    SimpleRouter::get('/AllMeetup', 'MeetupController@getAll');
    SimpleRouter::post('/DeleteEvent', 'MeetupController@deleteEvent')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/AddEvent', 'MeetupController@addEvent')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/GetEventById', 'MeetupController@getEventById')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/UpdateEvent', 'MeetupController@updateEvent')->addMiddleware(APIMiddleware::class);
    //Location
    SimpleRouter::get('/AllLocation', 'LocationController@getAll');
});