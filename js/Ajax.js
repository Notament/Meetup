//Toggle
$('#connexion').click(function(){
    $('.form_connexion').toggle();
})

//LOGIN
formLog = document.querySelector('.event');
formLog.addEventListener('submit', log_user);
$('form').removeClass('event');

function log_user(event){
    event.preventDefault();
    form = new FormData(this);
    $.ajax({
        url: "Api/login",
        method : 'POST',
        data : form, 
        processData : false, 
        contentType : false, 
      }).done(function(msg) {
          toastr.success("Vous êtes connectés");
          json = JSON.parse(msg);
        localStorage.setItem("token",json.token);
      }).fail(function(){
          toastr.error("Vos identifiants sont incorrectent");
      })

}

//SHOW EVENT
window.onload = function (){
    $.ajax({
        url: "Api/AllMeetup",
      }).done(function(msg) {
            json = JSON.parse(msg);
            json.forEach(function(element){
                $('.meetups').append("<div class='meetup' id='"+ element[0]+"'</div>");
                $('.meetup:last').append("<form method='POST' class='update_event event'>Voici le meetup "+ element['meetup_title']+"<br> Le meetup commence le " + element['meetup_date']+" à "+ element['meetup_hour']+" h<br>Et se trouve à "+ element['city']+" à l'adresse "+ element['address'] +"<br><img src="+ element['meetup_pic']+"><input type='hidden' name='id' value='"+ element[0] +"'><input type='submit' value='Edit'></form>");
                var formA = document.querySelector('.event');
                formA.addEventListener('submit', edit_event);
                $('form').removeClass('event');
                $('.meetup:last').append("<form method='POST' class='delete_event event'> <input type='hidden' name='id' value='"+ element[0] +"'><input type='submit' value='Delete'></form>")
                    var formA = document.querySelector('.event');
                    formA.addEventListener('submit', delete_event);
                    $('form').removeClass('event');
                $('.meetup:last').append(
                    `<form method="POST" class="event">
                        <input type="hidden" value="${element[0]}"name="id">
                        <input type="submit" value="Voir le meetup">
                    </form>`
                );
                var formA = document.querySelector('.event');
                formA.addEventListener('submit', show_event);
                $('form').removeClass('event');
            })
            $.ajax({ 
                url: "Api/AllLocation", 
            }).done(function(msg){
                $('.location').empty();
                $('.location').append("<select name='location' class='event_location' required>");
                json = JSON.parse(msg);
                json.forEach(function(element){
                    $('.event_location').append("<option value='"+ element['city'] +"'>"+ element['address'] +" "+ element['city'] +" "+ element['zip_code'] +"</option>");
                })
                $('.container').css({'display' : 'none'});
            })
      });
      
}

//DELETE EVENT
function delete_event(event){
    $("input[type=submit]").prop("disabled", true);
    $('.container').css({'display' : 'block'});
    token = localStorage.getItem('token');
    event.preventDefault();
    var form = new FormData(this);
    form.append('token',token);
    $.ajax({ 
        method : 'POST', 
        url: 'Api/DeleteEvent',
        data : form,
        processData : false, 
        contentType : false, 
    }).done(function(msg){
        id_form = form.get('id');
        form_remove = document.getElementById(id_form);
        form_remove.remove();
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.success("L'event a bien été supprimé");
    }).fail(function(){
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.error("Vous n'êtes pas authentified");
    })
};

//ADD EVENT
var formC = document.querySelector('.add_event');
formC.addEventListener('submit', add_event);

function add_event(event){
    $("input[type=submit]").prop("disabled", true);
    $('.container').css({'display' : 'block'});
    event.preventDefault();
    var form = new FormData(this);
    token = localStorage.getItem('token');
    form.append('token',token);
    $.ajax({
        method : 'POST',
        url: "Api/AddEvent",
        data : form,
        processData : false,
        contentType : false,
    }).done(function(msg){
        json = JSON.parse(msg);
                $('.meetups').append("<div class='meetup' id='"+ json[0][0]+"'></div>");
                $('.meetup:last').append("<form method='POST' class='update_event event'>Voici le meetup "+ json[0]['meetup_title']+"<br> Le meetup commence le " + json[0]['meetup_date']+" à"+ json[0]['meetup_hour'] +" h<br>Et se trouve à "+ json[0]['city']+" à l'adresse "+ json[0]['address'] +"<br><br><img src='"+ json[0]['meetup_pic']+"'><input type='hidden' name='id' value='"+ json[0][0] +"'><input type='submit' value='Edit'></form>");
                    var formA = document.querySelector('.event');
                    formA.addEventListener('submit', edit_event);
                    $('form').removeClass('event');
                $('.meetup:last').append("<form method='POST' class='delete_event event'> <input type='hidden' name='id' value='"+ json[0][0] +"'><input type='submit' value='Delete'></form>")
                    var formA = document.querySelector('.event');
                    formA.addEventListener('submit', delete_event);
                    $('form').removeClass('event');
                    $('.meetup:last').append(
                        `<form method="POST" class="event">
                            <input type="hidden" value="${json[0][0]}"name="id">
                            <input type="submit" value="Voir le meetup">
                        </form>`
                    );
                    var formA = document.querySelector('.event');
                    formA.addEventListener('submit', show_event);
                    $('form').removeClass('event');
                    $('.container').css({'display' : 'none'});
                    toastr.success('Event bien rajouté à notre planning');
                    $("input[type=submit]").prop("disabled", false);
    }).fail(function(){
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.error("Vous n'êtes pas authentified");
    })

}

//EDIT UN EVENT
function edit_event(event){
    $("input[type=submit]").prop("disabled", true);
    $('.container').css({'display' : 'block'});
    event.preventDefault();
    var form = new FormData(this);
    token = localStorage.getItem('token');
    form.append('token',token);
    $.ajax({ 
        method : 'POST', 
        url: "Api/GetEventById", 
        data : form, 
        processData : false, 
        contentType : false, 
    }).done(function(msg){
        $('.meetups').empty();
        $('.add_meetup').css({'display' : 'none'});
        json = JSON.parse(msg);
        $('.current_meetup').append("<form method='POST' class='edit_event event'><input type='text' name='meetup_title' value='"+ json[0]['meetup_title']+"'required><br><textarea name='meetup_description' rows='4' cols='50' required>"+ json[0]['meetup_description'] +"</textarea><br><input type='number' name='hour' min='0' max='24' value='"+ json[0]['meetup_hour'] +"' required><br><input type='date' name='daymonth' value='"+ json[0]['meetup_date'] +"' required><br><div class='location'></div><br><input type='hidden' name='id' value='"+ json[0]['id'] +"'><input type='submit' value='Update'></form>");
        var formA = document.querySelector('.event');
        formA.addEventListener('submit', update_event);
        $('form').removeClass('event');
        $("input[type=submit]").prop("disabled", true);
        $.ajax({ 
            url: "Api/AllLocation", 
        }).done(function(msg){
            $('.location').empty();
            $('.location').append("<select name='location' class='event_location' required>");
            json = JSON.parse(msg);
            json.forEach(function(element){
                $('.event_location').append("<option value='"+ element['city'] +"'>"+ element['address'] +" "+ element['city'] +" "+ element['zip_code'] +"</option>");
            })
            $('.container').css({'display' : 'none'});
            $("input[type=submit]").prop("disabled", false);
        })
    }).fail(function(){
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.error("Vous n'êtes pas authentified");
    })
}

//UPDATE D'UN EVENT
function update_event(event){
    $("input[type=submit]").prop("disabled", true);
    $('.container').css({'display' : 'block'});
    event.preventDefault();
    var form = new FormData(this);
    token = localStorage.getItem('token');
    form.append('token',token);
    $.ajax({ 
        method : 'POST', 
        url: "Api/UpdateEvent", 
        data : form, 
        processData : false, 
        contentType : false, 
    }).done(function(msg){
        $('.current_meetup').empty();
        $('.meetups').empty();
        $('.meetups').css({'display' : 'block'});
        $('.add_meetup').css({'display' : 'block'});
        $.ajax({
            url: "Api/AllMeetup",
          }).done(function(msg) {
                json = JSON.parse(msg);
                json.forEach(function(element){
                    $('.meetups').append("<div class='meetup' id='"+ element[0]+"'</div>");
                    $('.meetup:last').append("<form method='POST' class='update_event event'>Voici le meetup "+ element['meetup_title']+"<br> Le meetup commence le " + element['meetup_date']+" à "+ element['meetup_hour'] +" h<br>Et se trouve à "+ element['city']+" à l'adresse "+ element['address'] +"<br><br><img src="+ element['meetup_pic']+"><input type='hidden' name='id' value='"+ element[0] +"'><input type='submit' value='Edit'></form>");
                    var formA = document.querySelector('.event');
                    formA.addEventListener('submit', edit_event);
                    $('form').removeClass('event');
                    $('.meetup:last').append("<form method='POST' class='delete_event event'> <input type='hidden' name='id' value='"+ element[0] +"'><input type='submit' value='Delete'></form>")
                        var formA = document.querySelector('.event');
                        formA.addEventListener('submit', delete_event);
                        $('form').removeClass('event');
                        $('.meetup:last').append(
                            `<form method="POST" class="event">
                                <input type="hidden" value="${element[0]}" name="id">
                                <input type="submit" value="Voir le meetup">
                            </form>`
                        );
                        var formA = document.querySelector('.event');
                        formA.addEventListener('submit', show_event);
                        $('form').removeClass('event');
                })
                $('.container').css({'display' : 'none'});
                $("input[type=submit]").prop("disabled", false);
                toastr.success('Modification de votre event bien pris en compte');
          });
    }).fail(function(){
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.error("Vous n'êtes pas authentified");
    }) 

}

//AFFICHER LA PAGE D'UN EVENT

function show_event(event){
    $("input[type=submit]").prop("disabled", true);
    $('.container').css({'display' : 'block'});
    event.preventDefault();
    form = new FormData(this);
    token = localStorage.getItem('token');
    form.append('token',token);
    $.ajax({
        url: "Api/GetEventById",
        method : "POST",
        data : form,
        processData : false,
        contentType : false,
    }).done(function(msg){
        json = JSON.parse(msg);
        $('.meetups').css({'display':'none'})
        $('.add_meetup').css({'display':'none'})
        $('.current_meetup').append(`
        <input type="button" value="Retour en arrière" onclick="back()">
                <h1>${json[0]['meetup_title']}</h1>
                <div>le ${json[0]['meetup_date']} à ${json[0]['meetup_hour']}</div>
                <div>Description : ${json[0]['meetup_description']}</div>
                <input type="button" value="Je participe">
                <div>${json[0]['address']}, ${json[0]['zip_code']} ${json[0]['city']}</div>
                <iframe
                    width="600"
                    height="450"
                    frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAFVsjqjp2Yn7tQuzH3q6prMlVdhi-_Fh0&q=${json[0]['city']}" allowfullscreen>
                </iframe>
            `);
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
    }).fail(function(){
        $("input[type=submit]").prop("disabled", false);
        $('.container').css({'display' : 'none'});
        toastr.error("Vous n'êtes pas authentified");
    })
}

function back(){
    $('.current_meetup').empty();
    $('.meetups').css({'display':'block'})
    $('.add_meetup').css({'display':'block'})
}